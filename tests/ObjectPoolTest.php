<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\DesignPatterns\Tests;

use Interactiv4\Contracts\ObjectPool\Api\ObjectPoolInterface;
use Interactiv4\ObjectPool\ObjectPool;
use PHPUnit\Framework\TestCase;

/**
 * Class ObjectPoolTest
 */
class ObjectPoolTest extends TestCase
{
    /**
     * Test class exists, instantiable without parameters and is an instance of its related interface
     */
    public function testInstanceOf()
    {
        $objectPool = new ObjectPool();
        $this->assertInstanceOf(ObjectPoolInterface::class, $objectPool);
    }

    /**
     * Test getAll
     */
    public function testGetAll()
    {
        $objects = [
            'object1' => new \stdClass(),
            'object2' => new \stdClass(),
        ];
        $objectPool = new ObjectPool($objects);
        $this->assertEquals($objects, $objectPool->getAll());
    }

    /**
     * Test get
     */
    public function testGet()
    {
        $objects = [
            'object1' => new \stdClass(),
            'object2' => new \stdClass(),
        ];
        $objectPool = new ObjectPool($objects);
        $this->assertSame($objects['object1'], $objectPool->get('object1'));
        $this->assertSame($objects['object2'], $objectPool->get('object2'));
        $this->assertNotSame($objects['object1'], $objectPool->get('object2'));
        $this->assertNotSame($objects['object2'], $objectPool->get('object1'));
    }

    /**
     * Test has
     */
    public function testHas()
    {
        $objects = [
            'object1' => new \stdClass(),
            'object2' => new \stdClass(),
        ];
        $objectPool = new ObjectPool($objects);
        $this->assertTrue($objectPool->has('object1'));
        $this->assertTrue($objectPool->has('object2'));
        $this->assertFalse($objectPool->has('object3'));
    }

    /**
     * Test array access
     */
    public function testArrayAccess()
    {
        $objects = [
            'object1' => new \stdClass(),
            'object2' => new \stdClass(),
        ];
        $objectPool = new ObjectPool($objects);

        // Test cast to array
        $this->assertEquals($objects, (array)$objectPool);
        // Test array access
        $this->assertSame($objects['object1'], $objectPool['object1']);
        $this->assertSame($objects['object2'], $objectPool['object2']);
        $this->assertFalse(isset($objectPool['object3']));

        $object4 = new \stdClass();
        $objectPool['object4'] = $object4;
        $this->assertTrue(isset($objectPool['object4']));
        $this->assertTrue($objectPool->has('object4'));
        $this->assertSame($object4, $objectPool->get('object4'));
    }
}
