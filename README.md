# Interactiv4 Object Pool

Description
-----------
This package provides implementation for interactiv4/object-pool-contracts package.


Versioning
----------
This package follows semver versioning.


Compatibility
-------------
- PHP ^7.2


Installation Instructions
-------------------------
It can be installed manually using composer, by typing the following command:

`composer require interactiv4/object-pool --update-with-all-dependencies`


Support
-------
Refer to [issue tracker](https://bitbucket.org/interactiv4/object-pool/issues) to open an issue if needed.


Credits
---------
Supported and maintained by Interactiv4 Team.


Contribution
------------
Any contribution is highly appreciated.
The best way to contribute code is to open a [pull request on Bitbucket](https://bitbucket.org/interactiv4/object-pool/pull-requests/new).


License
-------
[MIT](https://es.wikipedia.org/wiki/Licencia_MIT)


Copyright
---------
Copyright (c) 2019 Interactiv4 S.L.