<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\ObjectPool;

use Interactiv4\Contracts\ObjectPool\Api\ObjectPoolInterface;

/**
 * Class ObjectPool
 * @api
 */
class ObjectPool extends \ArrayObject implements ObjectPoolInterface
{
    /**
     * ObjectPool constructor.
     * @param \object[] $objects
     */
    public function __construct(
        array $objects = []
    ) {
        parent::__construct($objects);
    }

    /**
     * @inheritdoc
     */
    public function getAll(): array
    {
        return $this->getArrayCopy();
    }

    /**
     * @inheritdoc
     */
    public function has(string $name): bool
    {
        return $this->offsetExists($name);
    }

    /**
     * @inheritdoc
     */
    public function get(string $name): ?object
    {
        return $this->offsetGet($name);
    }
}
